#include <iostream>
#include <stdio.h>
#include <string.h>
#include <openssl/sha.h>
#include <chrono>
#include <cstdlib>
#include <ctime>
#include <random>
#include <map>
#include <iomanip>

using namespace std;

class Coin{
    public:
        Coin() = default;
        virtual bool coinArgs(int nbArgs, char* argv[]) = 0;
        virtual void benchmark() = 0;
        virtual string createToken(const string name) = 0;
        virtual string createNonce() = 0;
        virtual string toSha1(string token, int length) = 0;
};

class CoinCoin:public Coin{
    public: 

        float round(float time)  {
            //char str[40];
            //sprintf(str, "%.2f", time);
            //sscanf(str, "%f", &time);
            return ((int)(time * 100 + .5) / 100.0);
            //return time;
        };

        string createNonce() override {
            string nonce;
            string ascii_values = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890/*-+.!,?;:%*&~#{[`\\^@]}^$\"%<>_-|= ";
            for(int i=0; i<32; i++){
                nonce += ascii_values[rand()% (ascii_values.length()-1)];
            }
            return nonce;
        }

        string createToken(const string name) override {
            return createNonce() + "-" + name + "-CC1.0-" + to_string(static_cast<long int> (time(0))) + "-0f0f0f";
        }

        string toSha1(string token, int length) override {
            unsigned char hash[SHA_DIGEST_LENGTH];
            SHA1((unsigned char*) token.c_str(), length-1, hash);
            char str[SHA_DIGEST_LENGTH];
            for(int i=0; i<SHA_DIGEST_LENGTH;i++){
                sprintf(&str[i*2], "%02x", (unsigned int) hash[i]);
            }
            return str;
        }

        void benchmark() override {
            cout << "Starting benchmark..." << endl;
            string name = "ASC";
            bool stop = false;
            chrono::duration<double> time{};
            auto debut = chrono::system_clock::now();
            while (!stop)
            {
                string token = createToken(name);
                string coin = toSha1(token, token.length());
                if (coin.substr(0,6) == "cccccc") {
                    auto fin = chrono::system_clock::now();
                    time = fin-debut;
                    stop = true;
                }
            }
            cout << "6c (subcoin) mined in " <<  round(time.count()) <<" s" << endl;
            cout << "*** Mining projections ***" << endl;
            cout << "6c (subcoin)" << "\t\t" << round(time.count()) << "s" << "\t\t" << "(" << round(time.count()/3600) << "h)" << endl;
            cout << "7c (coin)" << "\t\t" << round(time.count()*16) << "s" << "\t\t" << "(" << round((time.count()*16)/3600) << "h)" << endl;
            cout << "8c (hexcoin)" << "\t\t" << round(time.count()*pow(16,2)) << "s" << "\t" << "(" << round((time.count()*pow(16,2))/3600) << "h)" << endl;
            cout << "9c (arkenstone)" << "\t\t" << round(time.count()*pow(16,3)) << "s" << "\t" << "(" << round((time.count()*pow(16,3))/3600) << "h)" << endl;
            cout << "10c (blackstar)" << "\t\t" << round(time.count()*pow(16,4)) << "s" << "\t" << "(" << round((time.count()*pow(16,4))/3600) << "h)" << endl;
            cout << "11c (grand cross)" << "\t" << round(time.count()*pow(16,5)) << "s" << "\t" << "(" << round((time.count()*pow(16,5))/3600) << "h)" << endl;
        }  

        inline bool coinArgs(int nbArgs, char* argv[]) override {
            try {
                if (nbArgs<2) {
                    throw string("You did not specify any arguments... Please add option -z");
                } else if (nbArgs>2) {
                    throw string("You specify too much arguments... Please use option -z");
                } else {
                    if ((string)argv[1]== "-z") {
                        return true;
                    }else{
                        throw string("You did not specify valid arguments... Please use option -z");
                    }
                }
            } catch (string err) {
                cerr << err << endl;
                return false;
            }
        }
};

int main(int argc, char** argv) {
    srand(time(0));
    auto *coin = new CoinCoin();
    bool hasArgs = coin->coinArgs(argc, argv);
    if (hasArgs) {
        coin->benchmark();
    }else{
        return 1;
    }
    return 0;
}