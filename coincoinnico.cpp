#include <iostream>
#include <map>
#include <stdio.h>
#include <string.h>
#include <random>
#include <ctime>
#include <openssl/sha.h>
#include <chrono>
#include <cstdlib>

using namespace std;

class CoinMiner{
    public:
        CoinMiner() = default;
        virtual char getchar() = 0;
        virtual string getsha(size_t taille, unsigned char *chaine) = 0;
        virtual string getnonce() = 0;
        virtual long int getchrono() = 0;
        virtual string gettoken(const string essai) = 0;
        virtual inline bool verif(int intervalle) = 0;
        virtual inline int getlec(int seuil, string pieces, string token) = 0;
        virtual void benchmark() = 0;
        virtual void minage(string tri, int min) = 0;
        virtual map<string, string> mintri(char* argv[]) = 0;
        virtual void menu(char* argv[]) = 0;
};

class CoinCoinMiner : public CoinMiner{
    public:
        char table[97];
        int tailleA;
        CoinCoinMiner(){
            string asci =
                "AZERTYUIOPQSDFGHJKLMWXCVBN"
                "azertyuiopqsdfghjklmwxcvbn"
                "0123456789/*-+.!,?;:%*&~#{["
                "`\\^@]}^$\"%<>_-|= ";
            strcpy(table, asci.c_str());
            tailleA = sizeof(table) - 1;
        };

        char getchar() override{
            return table[rand() % tailleA];
        }

        string getsha(size_t taille, unsigned char *chaine) override{
            char tampon[SHA_DIGEST_LENGTH];
            unsigned char hash[SHA_DIGEST_LENGTH];
            SHA1(chaine, taille, hash);
            for(int i = 0; i < SHA_DIGEST_LENGTH; i++)
                sprintf(&tampon[i * 2], "%02x", (unsigned int) hash[i]);
            return tampon;
        }

        string getnonce() override{
            string chaine;
            for(int i = 0; i < 32; i++){
                chaine = chaine + getchar();
            }
            return chaine;
        }

        long int getchrono() override{
            time_t tps = time(0);
            long timestamp = static_cast<long int> (tps);
            return timestamp;
        }

        string gettoken(const string essai) override{
            string token = getnonce() + "-" + essai + "-CC1.0-" + to_string(getchrono()) + "-0f0f0f";
            return token;
        }

        inline bool verif(int intervalle) override {
            try {
                if (intervalle < 2 || intervalle > 6)
                    throw string("Mauvais arguments !");
                else
                    return true;
            } catch (string const& error) {
                cerr << error << endl;
                return false;
            }
        }

        inline int getlec(int seuil, string pieces, string token) override{
            int nbc = 0;
            unsigned int i = 0;
            while(i < pieces.length()){
                if(pieces[i] == 'c'){
                    nbc++;
                    i++;
                }else
                    i = pieces.length();
            }
            if(nbc >= seuil){
                cout << nbc << " un c" << endl;
                cout << pieces << endl;
                cout << token << endl;
            }
            return nbc;
        }

        void benchmark() override{
            bool boule = true;
            cout << "Starting benchmark..." << endl;
            string coin;
            auto start = chrono::system_clock::now();
            chrono::duration<double> elapsed_seconds{};
            while (boule) {
                string token = gettoken("NLE");
                char chaine[token.length()+1];
                strcpy(chaine, token.c_str());
                size_t tailleN = sizeof(chaine)-1;
                coin = getsha(tailleN, (unsigned char*) chaine);
                if (coin.substr(0,6) == "cccccc") {
                    auto end = chrono::system_clock::now();
                    elapsed_seconds = end-start;
                    boule = false;
                }
            }
            cout << "Benchmark executed in " << elapsed_seconds.count() << "s\n";
            elapsed_seconds = elapsed_seconds;
            cout << "6c (subcoin) mined in " << elapsed_seconds.count() << "s\n";
            cout << "*** Mining projections ***" << "\n";
            cout << "subcoin" << "\t\t" << elapsed_seconds.count() << " s" << "\t" << (elapsed_seconds.count()*16)/3600 << " h" <<"\n";
            cout << "coin" << "\t\t" << elapsed_seconds.count()*pow(16,1) << " s" << "\t" << (elapsed_seconds.count()*pow(16,2))/3600 << " h" <<"\n";
            cout << "hexcoin" << "\t\t" << elapsed_seconds.count()*pow(16,2) << " s" << "\t" << (elapsed_seconds.count()*pow(16,3))/3600 << " h" <<"\n";
            cout << "arkenstone" << "\t" << elapsed_seconds.count()*pow(16,3) << " s" << "\t" << (elapsed_seconds.count()*pow(16,4))/3600 << " h" <<"\n";
            cout << "blackstar" << "\t" << elapsed_seconds.count()*pow(16,4) << " s" << "\t" << (elapsed_seconds.count()*pow(16,5))/3600 << " h" <<"\n";
            cout << "grand cross" << "\t" << elapsed_seconds.count()*pow(16,5) << " s" << "\t" << (elapsed_seconds.count()*pow(16,6))/3600 << " h" <<"\n";
        }

        void minage(string tri, int min) override {

            bool boule = true;
            string token;
            while(boule){
                string token = gettoken(tri);
                char data[token.length()+1];
                strcpy(data, token.c_str());
                size_t length = sizeof(data)-1;
                string test = getsha(length, (unsigned char*)data);
                getlec(min, test, token);
            }
        }

        map<string, string> mintri(char* argv[]) override {
            map<string, string> params;
            params["tri"] = argv[2];
            params["min"] = argv[4];
            return params;
        }

        void menu(char* argv[]) override {
            if((string)argv[1] == "-z"){
               benchmark();
            }else{
                map<string, string> params = mintri(argv);
                minage(params["tri"], stoi(params["min"]));
            }
        }

};

int main(int argc, char* argv[]) {
    srand(time(0));
    auto *miner = new CoinCoinMiner();
    bool commandLineTrue = miner->verif(argc);
    if (!commandLineTrue) {
        return 1;
    }else{
        miner->menu(argv);
    }

    return 0;
}